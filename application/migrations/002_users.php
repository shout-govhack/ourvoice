<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_users extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'creation_date TIMESTAMP DEFAULT \'0000-00-00 00:00:00\' NOT NULL',
			'modified_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL',
			'last_access_date' => array(
				'type' => 'DATETIME'
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 64
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => 32
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => 32
			),
			'level' => array(
				'type' => 'VARCHAR',
				'constraint' => 16
			),
			'last_ip' => array(
				'type' => 'VARCHAR',
				'constraint' => 16
			),
			'blocked' => array(
				'type' => 'TINYINT',
				'constraint' => 3,
				'default' => 1
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');
		
		$data = array(
			array(
				'creation_date' => date('Y-m-d H:i:s'),
				'name' => 'Administrator',
				'username' => 'admin',
				'password' => md5('admin'),
				'level' => 'ADMIN',
				'blocked' => '0'
			)
		);
		
		$this->db->insert_batch('users', $data);
	}

	public function down()
	{
		$this->dbforge->drop_table('users');
	}
}