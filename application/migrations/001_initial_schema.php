<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'service_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE
			),
			'date' => array(
				'type' => 'DATETIME'
			),
			'service_location_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE
			),
			'interaction_date' => array(
				'type' => 'DATETIME'
			),
			'location_lat' => array(
				'type' => 'DECIMAL',
				'constraint' => '11, 8'
			),
			'location_lng' => array(
				'type' => 'DECIMAL',
				'constraint' => '11, 8'
			),
			'service_rating' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE
			),
			'attitude_rating' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE
			),
			'confidence_rating' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE
			),
			'comments' => array(
				'type' => 'TEXT'
			),
			'ip' => array(
				'type' => 'VARCHAR',
				'constraint' => 16
			),
			'approved' => array(
				'type' => 'TINYINT',
				'constraint' => 3
			),
		));
		
//		CREATE TABLE IF NOT EXISTS `our-voice`.`feedback` (
//			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//			`service_id` INT UNSIGNED NULL,
//			`date` DATETIME NULL,
//			`service_location_id` INT UNSIGNED NULL,
//			`interaction_date` DATETIME NULL,
//			`location_lat` DECIMAL(11,8) NULL,
//			`location_lng` VARCHAR(45) NULL,
//			`service_rating` INT UNSIGNED NULL,
//			`attitude_rating` INT UNSIGNED NULL,
//			`confidence_rating` INT UNSIGNED NULL,
//			`comments` TEXT NULL,
//			`ip` VARCHAR(16) NULL,
//			`approved` TINYINT UNSIGNED NULL,
//			PRIMARY KEY (`id`))
//		  ENGINE = InnoDB

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('feedback');
	}

	public function down()
	{
		$this->dbforge->drop_table('feedback');
	}
}