<?php

function check_auth(){
	$CI = &get_instance();
	if (!$CI->session->userdata('logged_in')) {
		//If no session, redirect to login page
		redirect('auth/login', 'refresh');
	}
}

function get_auth_data(){
	$CI = &get_instance();
	return $CI->session->userdata('logged_in');
}

function is_logged_in(){
	$CI = &get_instance();
	return $CI->session->userdata('logged_in') ? true : false;
}
