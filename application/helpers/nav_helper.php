<?php

/**
 * 
 * @param string $title
 * @param string $url
 * @param string $active
 * @return string
 */
function nav_list_item($title, $url, $active){
	
	$slug = strtolower(str_replace(' ', '-', $title));
	$activeHtml = $active == $slug ? ' class="active"' : '';
	$screenReaderHtml = $active == $slug ? ' <span class="sr-only">(current)</span>' : '';
	
	return '<li' . $activeHtml . '><a href="' . $url . '">' . strtoupper($title) . $screenReaderHtml . '</a></li>';
}