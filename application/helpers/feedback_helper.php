<?php

function feedback_rating_percentage($feedback){
	return round((($feedback->service_rating
			+ $feedback->attitude_rating
			+ $feedback->confidence_rating) / 15) * 100) . '%';
}

function feedback_time_ago($feedback){
	$etime = time() - strtotime($feedback->date);

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function feedback_percentage($givenVal, $maxVal){
	return round(($givenVal / $maxVal) * 100) . '%';
}

function feedback_dots_widget($percentage) {
	$percentage = str_replace('%', '', $percentage);
	
	$html = '<div class="rating-widget">
		<div class="rating-input-empy">
			<i class="glyphicon glyphicon-star-empty" data-value="1"></i>
			<i class="glyphicon glyphicon-star-empty" data-value="2"></i>
			<i class="glyphicon glyphicon-star-empty" data-value="3"></i>
			<i class="glyphicon glyphicon-star-empty" data-value="4"></i>
			<i class="glyphicon glyphicon-star-empty" data-value="5"></i>
		</div>
		<div class="rating-input-full" style="width:' . (($percentage / 100) * 117) . 'px;">
			<div class="inner">
				<i class="glyphicon glyphicon-star" data-value="1"></i>
				<i class="glyphicon glyphicon-star" data-value="2"></i>
				<i class="glyphicon glyphicon-star" data-value="3"></i>
				<i class="glyphicon glyphicon-star" data-value="4"></i>
				<i class="glyphicon glyphicon-star" data-value="5"></i>
			</div>
		</div>
	</div>';
	return $html;
}

