<?php

class Feedback_model extends CI_Model {

	public $id;
	public $service_id;
	public $date;
	public $interaction_date;
	public $location_lat;
	public $location_lng;
	public $service_rating;
	public $attitude_rating;
	public $confidence_rating;
	public $comments;
	public $ip;
	public $approved;

	public function __construct() {
		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get_last_ten_entries() {
		$this->db->from('feedback');
		$this->db->where('approved', true);
		$this->db->order_by("date", "desc");
		$this->db->limit(10, 0);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_entries_page($page, $perPage){
		$this->db->from('feedback');
		$this->db->order_by("date", "desc");
		$this->db->limit($perPage, ($page - 1) * $perPage);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function approve_feedback($ids, $shouldApprove){
		$this->db->where_in('id', $ids);
		$this->db->update('feedback', array('approved' => $shouldApprove));
	}
	
	public function delete_entries($ids){
		$this->db->where_in('id', $ids);
		$this->db->delete('feedback');
	}

	public function insert_entry() {
		$this->service_id = 1;
		$this->date = date('Y-m-d H:i:s', time());
		$this->approved = false;
		
		$this->db->insert('feedback', $this);
	}
}
