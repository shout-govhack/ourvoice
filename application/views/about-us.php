<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
$this->load->view('partials/header', array(
	'active' => 'about-us',
	'title' => 'About us',
	'class' => 'static'
));
?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>About us</h1>
			<h2>How things got started</h2>
			<p>
				OurVoice.org.nz has been launched as part of a #GovHack2016 project in July 2016. 
			</p>	
			<p>
				Our team of seven came together to collaborate with skills such as development, 
				design, production and more. 
			</p>	
			<p>
				On Friday night, we pretty quickly agreed to the idea that, rather than just ‘take’ 
				data from the government, we wanted to create a virtuous loop to help make New 
				Zealand a better place for everyone. 
			</p>	
			<p>
				By pulling information from government services, we can serve this up to the nation 
				to provide feedback on the experience they’ve had. An independent third-party think 
				tank can then analyse the data and provide government services with insights to 
				help them drive efficiencies and improvements. 
			</p>	
			<p>
				<strong>
					Kiwis pay $66 billion every year using government services – if we could improve 
					things even just by 1%, this would save our country approximately $660 million every year!
				</strong>
			</p>	
			<p>
				As tax-payers, this is a cause that is very close to our hearts.
			</p>	
			<p>
				Thanks for stopping by!
			</p>	
			<p>
				Tineke, Tansy, Adi, Cain, Ryan, Chris and Tasneem
			</p>
		</div>
	</div>

</div>

<?php
$this->load->view('partials/footer', array(
	'active' => 'about-us'
));
?>