<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
$this->includeScripts = array(
	base_url('public/lib/bootstrap-rating-input.min.js'),
	base_url('public/js/location-selector.js'),
	'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_API_KEY . '&callback=initMap' => 'async defer'
);
?>

<?php $this->load->view('partials/header', array(
	'class' => 'police feedback-form'
)); ?>

<style>
	#map {
		height: 250px;
		position: relative;
		width: 100%;
	}

	.maps-frame {
		height: 250px;
		width: 100%;
	}
	
	#police_station {
		padding-top: 11px;
	}

	form .container {
		max-width: 750px;
	}
</style>

<div class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h1 class="h1">NZ POLICE</h1>

				<p>Got feedback for NZ Police? Rate a police station and help them to improve their service to you and the rest of NZ.</p>
				<a href="<?php echo base_url() ?>police/home" class="btn btn-info">&lsaquo; GO BACK</a>
			</div>
			<div class="col-md-6">
				<img src="/public/images/police-rating.gif" alt="NZ Police">
			</div>
		</div>
	</div>
</div>

<form action="<?php echo base_url('/feedback/process') ?>" id="feedback" method="POST">
	<div class="container-fluid">

		<div class="form-group row">
			<!--				<div class="col-sm-12 title">
								<label>Where did this interaction take place?</label>
							</div>-->

			<div id="map"></div>
			<input type="hidden" name="interaction_lat" id="interaction_lat" value="" />
			<input type="hidden" name="interaction_lng" id="interaction_lng" value="" />

		</div>
	</div>


	<div class="container">
		<div class="form-group row">
			<div class="col-sm-6">
				<h4>
					NEAREST POLICE STATION
				</h4>
			</div>
			<div class="col-sm-6">
				<label id="police_station" class="small">Please select an interaction location on the map</label>
				<input type="hidden" name="service_location_id" id="service_location_id" value="" />
			</div>
		</div>
	</div>


	<div class="container-fluid section section-shaded">
		<div class="row">
			<div class="container">


				<!--	<div class="form-group row">
						<div class="col-sm-12">
							<label>
								Selected location: <span id="lat"></span> latitude, <span id="lng"></span> longitude
							</label>
						</div>
					</div>-->

				<div class="form-group row">
					<div class="col-sm-6 title">
						<label>When did this interaction take place?</label>
					</div>
					<div class="col-sm-6 field date-selector">
						<div class="form-group row">
							<div class="col-xs-4">
								<select class="form-control day" name="interaction_day">
									<option disabled selected>Day</option>
									<?php for ($i = 1; $i <= 31; $i++) { ?>
										<option value="<?php echo $i ?>"><?php echo $i ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-xs-4">
								<select class="form-control month" name="interaction_month">
									<option disabled selected>Month</option>
									<?php for ($i = 1; $i <= 12; $i++) { ?>
										<option value="<?php echo str_pad($i, 2, '0', STR_PAD_LEFT) ?>"><?php echo date('F', strtotime("2000-$i-01")) ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-xs-4">
								<select class="form-control year" name="interaction_year">
									<option disabled selected>Year</option>
									<?php for ($i = date('Y'); $i >= (date('Y') - 1); $i--) { ?>
										<option value="<?php echo $i ?>"><?php echo $i ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<input type="hidden" name="interaction_hour" value="12" />
						<input type="hidden" name="interaction_minute" value="00" />
						<input type="hidden" name="interaction_ap" value="pm" />
<!--						<div class="form-group row">
							<div class="col-xs-4">
								<select class="form-control hour" name="interaction_hour">
									<option disabled selected>Hour</option>
									<?php for ($i = 1; $i <= 12; $i++) { ?>
										<option value="<?php echo $i ?>"><?php echo $i ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-xs-4">
								<select class="form-control minute" name="interaction_minute">
									<option disabled selected>Minute</option>
									<?php for ($i = 0; $i < 60; $i += 15) { ?>
										<option value="<?php echo str_pad($i, 2, '0', STR_PAD_LEFT) ?>"><?php echo str_pad($i, 2, '0', STR_PAD_LEFT) ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-xs-4">
								<select class="form-control ap" name="interaction_ap">
									<option disabled selected>am/pm</option>
									<option value="am">am</option>
									<option value="pm">pm</option>
								</select>
							</div>
						</div>-->
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-6 title">
						<label>How would you rate your experience?</label>
					</div>
					<div class="col-sm-6 field rating-container">
						<div class="row">
							<div class="col-sm-6">Service</div>
							<div class="col-sm-6">
								<input type="number" name="service_rating" id="service_rating" class="rating" />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">Attitude</div>
							<div class="col-sm-6">
								<input type="number" name="attitude_rating" id="attitude_rating" class="rating" />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">Confidence</div>
							<div class="col-sm-6">
								<input type="number" name="confidence_rating" id="confidence_rating" class="rating" />
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-6 title">
						<label>Any comments you have on the interaction?</label>
					</div>
					<div class="col-sm-6 field">
						<textarea class="form-control" name="comments" id="comments" rows="15"></textarea>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-6">
						
					</div>
					<div class="col-sm-6">
						<button class="btn btn-info">SUBMIT MY FEEDBACK</button>
					</div>
				</div>
				<input type="hidden" name="service" value="police" />
			</div>
		</div>
	</div>

</form>

<?php $this->load->view('partials/footer', array()); ?>

