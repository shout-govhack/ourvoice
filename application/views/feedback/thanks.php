<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'class' => 'police static'
)); ?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-center">
			<h1 class="text-center" style="margin-top: 100px">Thanks a bunch!</h1>
			<h3 class="text-center">Your feedback is much appreciated.</h3>
			<p style="margin-bottom: 120px; margin-top: 30px;">
				<a href="<?php echo base_url('/police/home') ?>" class="btn btn-info">SEE MY FEEDBACK</a>
			</p>
		</div>
	</div>
</div>


<?php $this->load->view('partials/footer', array()); ?>
