<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'title' => 'Administration - Dashboard',
	'class' => 'dashboard static'
)) ?>

<?php $this->load->view('partials/admin-nav', array(
	'active' => 'dashboard'
)) ?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Dashboard</h1>
			<p>
				Yeah, yeah, I know there should be something here...
			</p>
		</div>
	</div>
</div>

<?php $this->load->view('partials/footer', array(

)) ?>