<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'title' => 'Administration - Dashboard',
	'class' => 'profile static'
)) ?>

<?php $this->load->view('partials/admin-nav', array(
	'active' => 'profile'
)) ?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Profile</h1>
			<form action="<?php echo base_url('admin/process-profile') ?>" method="POST">
				
				<div class="form-group row">
					<div class="col-sm-4">
						<label>Name:</label>
					</div>
					<div class="col-sm-8">
						<?php echo $profile['name'] ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-4">
						<label>Username:</label>
					</div>
					<div class="col-sm-8">
						<?php echo $profile['username'] ?>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-4">
						<label>Password:</label>
					</div>
					<div class="col-sm-8">
						<input class="form-control" type="password" name="password" />
					</div>
				</div>
				<button type="submit" class="btn btn-info">
					Save
				</button>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('partials/footer', array(

)) ?>