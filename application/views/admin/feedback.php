<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'title' => 'Administration - Dashboard',
	'class' => 'dashboard static'
)) ?>

<?php $this->load->view('partials/admin-nav', array(
	'active' => 'feedback'
)) ?>

<div class="container content">
	<form action="<?php echo base_url('admin/process-feedback') ?>" method="POST">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h1>Feedback</h1>

				<div class="form-inline actions">
					<span>With selected: </span>
					<select name="action" class="form-control">
						<option value="approve">Approve</option>
						<option value="unapprove">Unapprove</option>
						<option value="delete">Delete</option>
					</select>
					<button class="btn btn-info">Apply</button>
				</div>
				
				<hr />

				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th>Comment</th>
							<th>Service</th>
							<th>Date</th>
							<th>Approved</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($feedback as $f){ ?>
						<tr>
							<td>
								<input type="checkbox" id="check<?php echo $f->id ?>" value="<?php echo $f->id ?>" name="selected[]" />
							</td>
							<td><label for="check<?php echo $f->id ?>"><?php echo $f->comments ?></label></td>
							<td><?php echo $f->service_id == 1 ? 'POLICE' : '' ?></td>
							<td><?php echo $f->date ?></td>
							<td><?php echo $f->approved ? 'Yes' : 'No' ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>

		</div>
	</form>
</div>

<?php $this->load->view('partials/footer', array(

)) ?>