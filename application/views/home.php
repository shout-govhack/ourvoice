<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
	$this->includeScripts = array(
		base_url('public/lib/autocomplete.min.js'),
		base_url('public/js/service-select.js')
	);
?>
<?php $this->load->view('partials/header', array(
	'active' => 'home',
	'title' => 'Home'
)); ?>

<div class="page-header text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="h1">BETTER DATA. BETTER GOVERNMENT.</h1>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 tag-line text-center">
						<p>Want to have your say on the services our government provides? Now&nbsp;you can. Rate your experience and help make New&nbsp;Zealand a&nbsp;better place for everyone.</p>
					</div>
				</div>

				<form class="form-inline form-search" method="POST" id="serviceSearch">
					<div class="form-group">
						<label class="sr-only" for="search">Search</label>
						<input type="text" name="search" class="form-control search" id="search" placeholder="Please start typing 'Police' to see how this thing works&hellip;">
					</div>
					<button type="button" class="btn btn-info">GO</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="container">
			<div class="row slider text-center">
				<div class="col-md-3"><a href="<?php echo base_url() ?>police/home">
						<div class="slider-section">
							<span class="logo police"></span>
						</div>
						<p>Public satisfaction rating</p>
						<p><strong>72%</strong></p>
					</a>
				</div>
				<div class="col-md-3"><a href="<?php echo base_url() ?>police/home">
						<div class="slider-section">
							<span class="logo acc"></span>
						</div>
						<p>Public satisfaction rating</p>
						<p><strong>48%</strong></p>
					</a>
				</div>
				<div class="col-md-3"><a href="<?php echo base_url() ?>police/home">
						<div class="slider-section">
							<span class="logo ird"></span>
						</div>
						<p>Public satisfaction rating</p>
						<p><strong>64%</strong></p>
					</a>

				</div>
				<div class="col-md-3"><a href="<?php echo base_url() ?>police/home">
						<div class="slider-section">
							<span class="logo fire"></span>
						</div>
						<p>Public satisfaction rating</p>
						<p><strong>82%</strong></p>
					</a>
				</div>
			</div>
		</div>
	</div>
	
</div>

<section class="section section-shaded section-top">
	

	<section class="section">
		<h5 class="h4 text-center copy">Who does it best, who needs to improve and latest ratings from other Kiwis.</h5>
	</section>

	<div class="container">
		<div class="row text-center">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="h3">HIGHEST RATINGS</h3>
					</div>
					<div class="panel-body">
						<ul class="text-left no-bullets">
							<li><span class="perc">78%</span> <strong>NZ Police</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">68%</span> <strong>Ministry of Education</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">56%</span> <strong>IRD</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">55%</span> <strong>Work and income</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">53%</span> <strong>Immigration NZ</strong> <a href="#" class="pull-right">view</a></li>
						</ul>
					</div>
					<div class="panel-footer">
						<a href="#">View more</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="h3">LOWEST RATINGS</h3>
					</div>
					<div class="panel-body">
						<ul class="text-left no-bullets">
							<li><span class="perc">15%</span> <strong>NZ Customs</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">24%</span> <strong>Ministry of Health</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">32%</span> <strong>Child, Youth and Family</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">38%</span> <strong>ACC</strong> <a href="#" class="pull-right">view</a></li>
							<li><span class="perc">41%</span> <strong>Housing NZ</strong> <a href="#" class="pull-right">view</a></li>
						</ul>
					</div>
					<div class="panel-footer">
						<a href="#">View more</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="h3">LATEST RATING</h3>
					</div>
					<div class="row">
						<div class="col-xs-12" style="min-height: 200px;">
							<div class="col-xs-12">
								<h4 class="h4">NZ Police</h4>
								<?php echo feedback_dots_widget('67%') ?>
								<p class="text-left"><small>I had a burglary at home. The police were prompt to reply to the phone call and said they would send forensics in - took three days for them to respond. <strong>30 July 2016</strong></small></p>
							</div>
						</div>
						
					</div>
					<div class="panel-footer">
						<a href="#">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<h3 class="h3">HOW DOES MY FEEDBACK MAKE A DIFFERENCE?</h3>
						<p>Once you’ve left feedback we collate it in real time and provide insights back to the government. Then we help them to make things faster, cheaper and better than ever before.</p>
						<ul class="text-left" style="padding-left: 15px;margin-bottom: 20px;">
							<li>Help reduce crime</li>
							<li>Help speed up waiting times</li>
							<li>Help streamline processes</li>
						</ul>
						<button class="btn btn-info">FIND OUT MORE</button>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<h3 class="h3">WHAT’S IN IT FOR THE GOVERNMENT?</h3>
						<p>Acting on feedback from Kiwis will help the government leverage what’s great and improve what’s not – if we could improve services by just 1%, it would save our county $660 million every year.</p>

						<ul class="text-left" style="padding-left: 15px;margin-bottom: 20px;">
							<li>More trust</li>
							<li>More votes</li>
							<li>More money</li>
						</ul>

						<button class="btn btn-info">FIND OUT MORE</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>



<section class="section section-shaded">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<h2 class="h2">
					PUBLIC SATISFACTION WITH GOVERNMENT
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row text-center">
					<div class="col-md-4">
						<div class="circle">
							<p><small>JUNE</small></p>
							<h6 class="number number-orange">44%</h6>
							<a href="#"><small>View details</small></a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="circle">
							<p><small>THIS MONTH</small></p>
							<h6 class="number number-lightgreen">46%</h6>
							<a href="#"><small>View details</small></a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="circle">
							<p><small>LAST 12 MONTHS</small></p>
							<h6 class="number number-green">44%</h6>
							<a href="#"><small>View details</small></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<?php $this->load->view('partials/footer', array(
	'active' => 'home'
)); ?>
