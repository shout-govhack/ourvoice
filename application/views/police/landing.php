<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'class' => 'police'
)); ?>

<style>
	.feedback-stats .col-xs-2 {
		padding-left: 5px;
		padding-right: 5px;
	}
</style>

<div class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h1 class="h1">NZ POLICE</h1>

				<p>Got feedback for NZ Police? Rate a police station and help them to improve their service to you and the rest of NZ.</p>
				<a href="<?php echo base_url() ?>police/feedback" class="btn btn-info">GIVE FEEDBACK</a>
			</div>
			<div class="col-md-6">
				<img src="/public/images/police-rating.gif" alt="NZ Police">
			</div>
		</div>
	</div>
</div>

<section class="section section-shaded">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<p style="margin-top: 65px;">We've collected loads of trends from Kiwis throughout NZ.</p>
				<button class="btn btn-info btn-outline">VIEW TRENDS</button>
			</div>
			<div class="col-md-3 text-center">
				<div class="circle">
					<p><small>COMMENTS</small></p>
					<h6 class="number number-lightgreen">7.2K</h6>
					<a href="#"><small>Read</small></a>
				</div>
			</div>
			<div class="col-md-3 text-center">
				<div class="circle">
					<p><small>12 MONTHS AGO</small></p>
					<h6 class="number number-orange">52%</h6>
					<a href="#"><small>View details</small></a>
				</div>

			</div>
			<div class="col-md-3 text-center">
				<div class="circle">
					<p><small>YOY INCREASE</small></p>
					<h6 class="number number-green">+4%</h6>
					<a href="#"><small>View details</small></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h3 class="h3">LATEST COMMENTS</h3>
				<hr>
				
				<?php foreach($latest_feedback as $feedback){ ?>
				<div class="row">
					<div class="col-xs-12">
						<p>
							<?php echo $feedback->comments ?>
						</p>
						<p>
<!--							<small><?php echo feedback_time_ago($feedback) ?></small>-->
							<small><?php echo date('j F Y', strtotime($feedback->date)) ?></small>
						</p>
					</div>
				</div>
				<div class="row feedback-stats">
					<div class="col-xs-1">
						<strong><?php echo feedback_rating_percentage($feedback) ?></strong>
					</div>
					<div class="col-xs-3">
						<?php echo feedback_dots_widget(feedback_rating_percentage($feedback)) ?>
					</div>
					<div class="col-xs-2 small text-center">
						<small>Attitude | <strong><?php echo feedback_percentage($feedback->attitude_rating, 5) ?></strong></small>
					</div>
					<div class="col-xs-2 small text-center">
						<small>Confidence | <strong><?php echo feedback_percentage($feedback->confidence_rating, 5) ?></strong></small>
					</div>
					<div class="col-xs-2 small text-center">
						<small>Service | <strong><?php echo feedback_percentage($feedback->service_rating, 5) ?></strong></small>
					</div>
					<div class="col-xs-2 small text-center">
						<a href="#">View details</a>
					</div>
				</div>
				<hr />
				<?php } ?>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading tab-container">
						<div class="row">
							<div class="col-xs-12">
								<span class="tab selected">
									Overall
								</span>
								<span class="tab">
									Attitude
								</span>
								<span class="tab">
									Conf.
								</span>
								<span class="tab">
									Service
								</span>
							</div>
						</div>
						
					</div>
					<div class="panel-body">
						<h4 class="h4">West Coast <small class="pull-right">Change region</small></h4>
						<hr class="hr">
						<img src="<?php echo base_url('public/images/nz-map.gif') ?>" style="margin: 10px auto; display: block;" />
					</div>
					<ul class="list-group">
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-4">July 2015</div>
								<div class="col-md-3"><span class="icon icon-up"></span> <strong>3%</strong></div>
								<div class="col-md-2"><strong>49%</strong></div>
								<div class="col-md-3"><a href="">View</a></div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-4">July 2014</div>
								<div class="col-md-3"><span class="icon icon-down"></span> <strong>-2%</strong></div>
								<div class="col-md-2"><strong>52%</strong></div>
								<div class="col-md-3"><a href="">View</a></div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-4">July 2013</div>
								<div class="col-md-3"><span class="icon icon-up"></span> <strong>3%</strong></div>
								<div class="col-md-2"><strong>49%</strong></div>
								<div class="col-md-3"><a href="">View</a></div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-4">July 2012</div>
								<div class="col-md-3"><span class="icon icon-down"></span> <strong>-2%</strong></div>
								<div class="col-md-2"><strong>52%</strong></div>
								<div class="col-md-3"><a href="">View</a></div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="row">
								<div class="col-md-4">July 2011</div>
								<div class="col-md-3"><span class="icon icon-down"></span> <strong>-2%</strong></div>
								<div class="col-md-2"><strong>49%</strong></div>
								<div class="col-md-3"><a href="">View</a></div>
							</div>
						</li>
						


					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<?php $this->load->view('partials/footer', array()); ?>

