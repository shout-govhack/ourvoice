<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'active' => 'register',
	'title' => 'Register',
	'class' => 'static'
)); ?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Register with us</h1>
<!--			<?php echo validation_errors(); ?>
			<?php echo form_open(base_url('auth/process-registration'), 'class="register-form" method="POST"'); ?>
				<input type="text" name="username" placeholder="username"/>
				<input type="password" name="password" placeholder="password"/>
				<input type="text" name="email" placeholder="email address"/>
				<button>create</button>
				<p class="message">Already registered? <a href="#">Sign In</a></p>
			</form>-->
			<p>
				Soon, government services will be able to register on OurVoice.org.nz and access a range of features. These include:
			<p>
			<ul>
				<li>View trends for your agency, town and region</li>
				<li>Download reports and recommendations to help improve your services</li>
				<li>Discover unsung heroes and reward them for awesome service</li>
				<li>Respond to comments about your service</li>
				<li>Post helpful community information about your service</li>
			</ul>
			<p>
				Please come back in September 2016 to access these services.
			</p>
		</div>
	</div>
</div>

<?php $this->load->view('partials/footer', array(
	'active' => 'register'
)); ?>