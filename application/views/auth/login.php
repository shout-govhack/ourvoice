<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
$this->load->view('partials/header', array(
	'active' => 'login',
	'title' => 'Login',
	'class' => 'static'
));
?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Login</h1>
			<div class="login-page">
				<div class="form">
					<?php echo validation_errors(); ?>
					<?php echo form_open(base_url('auth/process'), 'class="login-form" method="POST"'); ?><form class="login-form">

						<input class="form-control" type="text" name="username" placeholder="username"/>
						<input class="form-control" type="password" name="password" placeholder="password"/>
						<button class="btn btn-info">login</button>
						<!--<p class="message">Not registered? <a href="#">Create an account</a></p>-->
					</form>
				</div>
			</div>
			<p>
				Soon, government services will be able to log in to OurVoice.org.nz and access a range of features. These include:
			</p>
			<ul>
				<li>View trends for your agency, town and region</li>
				<li>Download reports and recommendations to help improve your services</li>
				<li>Discover unsung heroes and reward them for awesome service</li>
				<li>Respond to comments about your service</li>
				<li>Post helpful community information about your service</li>
			</ul>
			<p>
				Please come back in September 2016 to access these services.
			</p>
		</div>
	</div>
</div>

<?php
$this->load->view('partials/footer', array(
	'active' => 'login'
));
?>