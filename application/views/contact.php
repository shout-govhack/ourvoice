<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'active' => 'contact',
	'title' => 'Contact',
	'class' => 'static'
)); ?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Contact</h1>
			<p>
				The best way to get in touch with us is on Facebook. Got a question? Ask us now.
			</p>
			<p>
				<a href="https://www.facebook.com/ourvoicenz/" target="_blank" class="btn btn-info">CHAT NOW</a>
			</p>
		</div>
	</div>
</div>

<?php $this->load->view('partials/footer', array(
	'active' => 'contact'
)); ?>