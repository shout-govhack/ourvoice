<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
$this->load->view('partials/header', array(
	'active' => 'where-to-from-here',
	'title' => 'Where to from here',
	'class' => 'static'
));
?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Where to from here</h1>
			<h2>We’re just getting started</h2>

			<p>
				Collecting data from open government sources is just the beginning. It’s what we’re able to help
				people do with that data that counts.
			</p>
			<p>
				In the near future we’d like to develop our #GovHack2016 project further:
			<ul>
				<li>
					<strong>Share statistics.</strong> Aggregate all feedback from Kiwis and share back on this website, so that
					we can see how our government services improve over time, and also discover where the
					best ones are.
				</li>
				<li>
					<strong>Overlay crime data.</strong> Bring in crime data and overlay it with government service location data
					– this will show where the hot spots are, and how the services nearby are rated. This
					correlation may help the government know where it needs to focus its efforts.
				</li>
				<li>
					<strong>Notify stakeholders.</strong>
					Access emails for charities and organisations to push information to
					them about government service statistics.
				</li>
				<li>
					<strong>Push to email contacts.</strong>
					We would contact these organisations or individuals directly to
					rate their experience. Specific case: Charities data can be downloaded to retrieve officer
					email addresses and phone numbers (~1,000,000 email addresses). These emails can be
					sent an email campaign requesting their feedback on dealings with the charities services
					department and IRD.
				</li>
				<li>
					<strong>Expand services to give feedback.</strong>
					Site is coded with scalability in mind – however we’ve
					limited our data collection to NZ Police for now. Expand services to include WINZ, IRD,
					MoH, MoD, Customs, Immigration and more.
				</li>
				<li>
					<strong>API goodness.</strong>
					Make the data we&#39;ve collected available through our own API.
				</li>
				<li>
					<strong>Service representative logins.</strong>
					Representatives from service branches (like a local police
					station) would have their own login in which they could reply to any of the comments
					directed to their station.
				</li>
				<li>
					<strong>Balanced views.</strong>
					Victim/offender data could be used to get both sides of the spectrum to
					rate their experiences with the police
				</li>
				<li>
					<strong>Non-anonymous feedback.</strong>
					Public user logins could be created to allow &quot;regular
					feedbackers&quot; to provide feedback more easily, or they may choose to provide feedback
					that&#39;s not anonymous. These users could be open to being contacted about their
					experience.
				</li>
				<li>
					<strong>Was this feedback helpful?</strong>
					Rating system like Amazon where users could flag another
					users feedback as helpful or not, which would then promote or demote feedback up or
					down the list.
				</li>
				<li>
					<strong>Administration area.</strong>
					 Admin area where feedback can be moderated and reviewed before being published.
				</li>
			</ul>
			</p>


			<h2>Got an idea for us?</h2>

			<p>
				There’s heaps we can do with open government data. If you have an idea, please let us know about

				it on <a href="https://www.facebook.com/ourvoicenz/" target="_blank">Facebook</a>.
			</p>


			<!--			<h2>Contact organisations or individuals directly to rate their experience</h2>
						<p>
							Any personal or organisation contact information that could be pulled out of government 
							data could be used to contact these entities directly and encourage them to rate
							their experience of the government service appropriate to them.<br /><br />
							Specific case: Charities data can be downloaded to retrieve officer email addresses and phone numbers (~1,000,000 email addresses).  
							These emails can be sent an email campaign requesting their feedback on dealings with the charities services department and IRD.
						</p>
						<p>
							eg. Victim/offender data could be used to get both sides of the spectrum to rate their 
							experiences with the police.  Charity data could be used to...
						</p>
						<h2>Expand services to give feedback to</h2>
						<p>
							Site is coded with scalability in mind.  Expand services to include...
						</p>
						<h2>API</h2>
						<p>
							Make the data that we've collected available through our own API.
						</p>
						<h2>Service representative logins</h2>
						<p>
							Representatives from service branches (like a local police station) would have their
							own login in which they could reply to any of the comments directed to their station.
						</p>
						<h2>Non-anonamous feedback</h2>
						<p>
							Public user logins could be created to allow "regular feedbackers" to provide
							feedback more easily, or they may choose to provided feedback that's not
							anonamous.  These users could be open to being contacted about their experience.
						</p>
						<h2>Was this feedback helpful?</h2>
						<p>
							Rating system like amazon where users could flag another users feedback as helpful
							or not, which would then promote or demote feedback up or down the list.
						</p>
						<h2>Administration area</h2>
						<p>
							Admin area where feedback can be reviewed before being set live.
						</p>-->
		</div>
	</div>
</div>

<?php
$this->load->view('partials/footer', array(
	'active' => 'where-to-from-here'
));
?>