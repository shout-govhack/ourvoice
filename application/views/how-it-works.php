<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('partials/header', array(
	'active' => 'how-it-works',
	'title' => 'How it works',
	'class' => 'static'
)); ?>

<div class="container content">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>How it works</h1>
			<h2>Here’s how OurVoice.org.nz works for Kiwis</h2>
			<ol>
				<li>
					<strong>Search for any NZ Government service.</strong> 
					We’ve pulled data from NZ’s public services – simply start typing in the <a href="<?php echo base_url() ?>">search box</a> and we’ll 
					do the rest. Once you find the service you want to review, find your location using the map.
				</li>
				<li>
					<strong>Leave your feedback.</strong>
					There are three ratings – service, safety and speed. Rate the service you’ve chosen, 
					then add additional comments if you’d like to. If you want to be contacted by the 
					service, please include your name and email. 
				</li>
				<li>
					<strong>We analyse all feedback from Kiwis.</strong>
					All feedback is collated in real time and analysed by an independent third-party 
					think tank. This means it’s impartial – by the people for the people. We’re analysing 
					feedback every day, so services get feedback faster.
				</li>
				<li>
					<strong>Great services are exemplified.</strong>
					Let’s say Otago Police Station has a higher rating than the national average. We 
					find out why, and then share these findings with other stations throughout the 
					country to help ALL police stations improve. 
				</li>
				<li>
					<strong>Insights are provided back to Government.</strong>
					Not only are we collecting data from the government and Kiwis throughout NZ, but 
					we’re creating a VIRTUOUS DATA LOOP by providing insights back to the government 
					to help improve services in NZ and save the government millions of dollars every year.
				</li>
				<li>
					<strong>Data is shared back to Kiwis too.</strong>
					Visit ourvoice.org.nz regularly to see how services in our country are improving 
					over time. Let’s be proud of our innovative, transparent culture – and our country-wide 
					effort to make NZ a better place for everyone.
				</li>
			</ol>
			<p>
				<img src="<?php echo base_url('public/images/loop.png') ?>" class="img-responsive" />
			</p>
		</div>
	</div>
	
</div>

<?php $this->load->view('partials/footer', array(
	'active' => 'how-it-works'
)); ?>