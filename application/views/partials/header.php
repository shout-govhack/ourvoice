<?php
	$this->migration->current();
	
	$active = isset($active) ? $active : null;
	$class = isset($class) ? $class : null;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" type="image/png" href="<?php echo base_url('public/images/favicon.png') ?>">

		<title>Our Voice - Better Data, Better Government</title>

		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<!--<link href="https://bootswatch.com/cerulean/bootstrap.min.css" rel="stylesheet">-->
		<link href="/public/css/ourvoice.css" rel="stylesheet">

		<?php
		if (isset($this->includeCss)) {
			foreach ($this->includeCss as $css) {
				echo "<link href=\"$css\" rel=\"stylesheet\">\n";
			}
		}
		?>


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		  <![endif]-->
    </head>

    <body class="page <?php echo $active . ' ' . $class ?>">
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
						m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-81621171-1', 'auto');
			ga('send', 'pageview');

		</script>

		<?php $this->load->view('partials/nav', array(
			'active' => $active
		)) ?>
