<?php
	$active = isset($active) ? $active : null;
?>

<footer class="footer">
	<div class="footer-blue">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					Why are we doing this? Kiwis pay $66 billion every year towards government services. If we could improve things even by just 1% - by giving Kiwis a voice and improving services as a result - it would save our country $660 million every year. ‪
					<strong><a href="https://www.facebook.com/hashtag/govhack2016?source=feed_text" target="_blank" style="padding-top: 10px;">#‎GovHack2016‬</a></strong>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default navfooter">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#footernav" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>

						<div class="collapse navbar-collapse" id="footernav">
							<ul class="nav navbar-nav">
								<?php 
								foreach($this->primaryNav as $title => $url){ 
									echo nav_list_item($title, $url, $active);
								}
								?>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
			</div>
		</div>
		<hr class="hr">
		<div class="row">
			<div class="col-sm-6 col-md-3">
				<h4 class="h4">MOST RATED SERVICES</h4>
				<p><small>NZ Police</small></p>
				<p><small>Ministry of Education</small></p>
				<p><small>IRD</small></p>
				<p><small>New Zealand Fire Service</small></p>
				<p><small>Immigration NZ</small></p>
				<p><small>NZ Customs</small></p>
				<p><small>Child Youth and Family</small></p>
				<p><small>ACC</small></p>
				<p><small>WINZ</small></p>
				<p><small>Housing NZ</small></p>
			</div>
			<div class="col-sm-6 col-md-3">
				<h4 class="h4">BEST SERVICE</h4>
				<p><small>NZ Police</small></p>
				<p><small>New Zealand Fire Service</small></p>
				<p><small>Ministry of Defence Forces</small></p>
				<p><small>IRD</small></p>
				<p><small>ACC</small></p>
				<p><small>Immigration NZ</small></p>
				<p><small>WINZ</small></p>
			</div>
			<div class="col-sm-6 col-md-3">
				<h4 class="h4">LEAST CONFIDENCE</h4>
				<p><small>Child Youth and Family</small></p>
				<p><small>Department of Corrections</small></p>
				<p><small>Ministry of Health</small></p>
				<p><small>Housing NZ</small></p>
			</div>
			<div class="col-sm-6 col-md-3">
				<h4 class="h4">BEST ATTITUDE</h4>
				<p><small>NZ Police</small></p>
				<p><small>New Zealand Fire Service</small></p>
				<p><small>Ministry of Defence</small></p>
				<p><small>WINZ</small></p>
				<p><small>IRD</small></p>
				<p><small>NZ Customs</small></p>
				<p><small>ACC</small></p>
			</div>
		</div>

		<div class="row text-center sub-footer">
			<div class="col-md-2 text-center social">
				<a href="https://www.facebook.com/ourvoicenz/" target="_blank"><img src="/public/images/social-facebook.png" class="img-responsive" /></a>
				<a href="https://twitter.com/ourvoice_nz" target="_blank"><img src="/public/images/social-twitter.png" class="img-responsive" /></a>
			</div>

			<div class="col-md-7">
				<p class="copyright"><small>&copy; ourvoice.org.nz <?php echo date('Y') ?></small></p>
			</div>
			<div class="col-md-3">
				<p><a class="navbar-brand" href="<?php echo base_url() ?>"></a></p>
			</div>
		</div>
	</div>
</footer>

<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<?php
		if(isset($this->includeScripts)){
			foreach($this->includeScripts as $script => $attr){
				$script = is_numeric($script) ? $attr : $script;
				echo "<script src=\"$script\"></script>\n";
			}
		}
	?>

</body>
</html>