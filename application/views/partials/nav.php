<?php
	$this->primaryNav = array(
		'Home' => base_url(),
		'How it works' => base_url('how-it-works'),
		'Where from here' => base_url('where-from-here'),
		'About us' => base_url('about-us'),
		'Contact' => base_url('contact')
	);
?>
	
<header class="header">
	<div class="ribbon-wrapper-yellow"><div class="ribbon-yellow">BETA</div></div>

	<div class="container">

		<div class="row">
			<div class="col-md-3 col-xs-6">
				<a href="<?php echo base_url() ?>" class="logo img-responsive"></a>
			</div>
			<div class="col-md-9 col-sx-6">
				<nav class="navbar navbar-default">


					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<?php
							foreach ($this->primaryNav as $title => $url) {
								echo nav_list_item($title, $url, $active);
							}
							?>
						</ul>

						<?php if(is_logged_in()){ ?>
						<ul class="nav navbar-nav navbar-right visible-lg">
							<li><a href="<?php echo base_url('auth/logout') ?>" class="btn btn-default navbar-btn">LOGOUT</a></li>
						</ul>
						<?php } else { ?>
						<ul class="nav navbar-nav navbar-right visible-lg">
							<li><a href="<?php echo base_url('auth/register') ?>">REGISTER</a></li>
							<li><a href="<?php echo base_url('auth/login') ?>" class="btn btn-default navbar-btn">LOGIN</a></li>
						</ul>
						<?php } ?>
					</div><!-- /.navbar-collapse -->

				</nav>
			</div>
		</div>

	</div>
</header>