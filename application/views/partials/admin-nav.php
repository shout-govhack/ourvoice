<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Our Voice Admin</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
				<?php 
				echo nav_list_item('Dashboard', base_url('admin/dashboard'), $active);
				echo nav_list_item('Feedback', base_url('admin/feedback'), $active);
				?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
				<?php 
					echo nav_list_item('Profile', base_url('admin/profile'), $active);
				?>
				<li><a href="<?php echo base_url('auth/logout') ?>">Logout</a></li>
            </ul>
		</div><!--/.nav-collapse -->
	</div><!--/.container-fluid -->
</nav>