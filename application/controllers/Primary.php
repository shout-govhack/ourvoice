<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Primary extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$this->load->view('home');
	}
	
	public function how_it_works(){
		$this->load->view('how-it-works');
	}
	
	public function about_us(){
		$this->load->view('about-us');
	}
	
	public function where_from_here(){
		$this->load->view('where-from-here');
	}
	
	public function contact(){
		$this->load->view('contact');
	}
}
