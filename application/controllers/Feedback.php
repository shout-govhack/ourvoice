<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {

	/**
	 * 
	 */
	public function __construct() {
        parent::__construct();
    }
	
	public function index()
	{
		$this->load->view('feedback/landing');
	}
	
	public function process() {
	
		$dateString = $this->input->post('interaction_year') . '-' .
				$this->input->post('interaction_month') . '-' .
				$this->input->post('interaction_day') . ' ' .
				$this->input->post('interaction_hour') . ':' .
				$this->input->post('interaction_minute') . ':00 ' .
				$this->input->post('interaction_ap');

		$interactionDate = date('Y-m-d H:i:s', strtotime(
				$dateString
			));
		
		$this->load->model('feedback_model');
		$this->feedback_model->service_location_id = $this->input->post('service_location_id');
		$this->feedback_model->location_lat = $this->input->post('interaction_lat');
		$this->feedback_model->location_lng = $this->input->post('interaction_lng');
		$this->feedback_model->interaction_date = $interactionDate;
		$this->feedback_model->service_rating = $this->input->post('service_rating');
		$this->feedback_model->attitude_rating = $this->input->post('attitude_rating');
		$this->feedback_model->confidence_rating = $this->input->post('confidence_rating');
		$this->feedback_model->comments = $this->input->post('comments');
		$this->feedback_model->ip = $this->input->server('REMOTE_ADDR');
		$this->feedback_model->insert_entry();

		redirect('feedback/thanks');
	}
	
	public function thanks(){
		$this->load->view('feedback/thanks');
	}
}
