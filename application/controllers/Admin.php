<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		if(!is_logged_in()){
			redirect(base_url('auth/login'), 'refresh');
		}
		
		$this->load->model('feedback_model');
	}

	/**
	 * 
	 */
	public function index() {
		redirect(base_url('admin/dashboard'));
	}
	
	/**
	 * 
	 */
	public function dashboard(){
		$this->load->view('admin/dashboard');
	}
	
	/**
	 * 
	 */
	public function feedback(){
//		$this->load->model('feedback_model');
		$page = $this->input->get('page');
		$page = is_null($page) ? 1 : $page;
		$feedback = $this->feedback_model->get_entries_page($page, 30);
		
		$this->load->view('admin/feedback', array(
			'feedback' => $feedback
		));
	}
	
	/**
	 * 
	 */
	public function process_feedback(){
		
		$selectedIds = $this->input->post('selected');
		$action = $this->input->post('action');
		
		switch($action){
			case 'approve':
				$this->feedback_model->approve_feedback($selectedIds, true);
				break;
			case 'unapprove':
				$this->feedback_model->approve_feedback($selectedIds, false);
				break;
			case 'delete':
				$this->feedback_model->delete_entries($selectedIds);
				break;
		}

		redirect(base_url('admin/feedback'));
	}
	
	public function profile(){
		$profile = get_auth_data();

		$this->load->view('admin/profile', array(
			'profile' => $profile
		));
	}
	
	public function process_profile(){
		$profile = get_auth_data();
		$password = $this->input->post('password');
		
		if(!is_null($password) && $password !== ''){
			$this->load->model('user');
			$this->user->update_password($profile['id'], $password);
		}
		
		redirect(base_url('admin/profile'), 'refresh');
	}
}