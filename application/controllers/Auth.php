<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user', '', TRUE);
	}

	/**
	 * 
	 */
	public function index() {
		redirect(base_url('auth/login'));
	}

	/**
	 * 
	 */
	public function login() {
		$this->load->helper(array('form'));
		$this->load->view('auth/login');
	}

	public function process() {
		//This method will have the credentials validation
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

		if ($this->form_validation->run() == FALSE) {
			//Field validation failed.  User redirected to login page
			$this->load->view('auth/login');
		} else {
			//Go to private area
			redirect(base_url('admin/dashboard'), 'refresh');
		}
	}

	function check_database($password) {
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');

		//query the database
		$result = $this->user->login($username, $password);

		if ($result) {
			$sess_array = array();
			foreach ($result as $row) {
				$sess_array = array(
					'id' => $row->id,
					'name' => $row->name,
					'username' => $row->username,
					'level' => $row->level
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		} else {
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}

	/**
	 * 
	 */
	public function logout() {
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect(base_url('auth/login'), 'refresh');
	}
	
	public function register(){
		$this->load->helper(array('form'));
		$this->load->view('auth/register');
	}

}
