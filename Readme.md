#Our Voice
## Shout

### Here’s how OurVoice.org.nz works for Kiwis

Search for any NZ Government service. We’ve pulled data from NZ’s public services – simply start typing in the search box and we’ll do the rest. Once you find the service you want to review, find your location using the map.

Leave your feedback. There are three ratings – service, safety and speed. Rate the service you’ve chosen, then add additional comments if you’d like to. If you want to be contacted by the service, please include your name and email.

We analyse all feedback from Kiwis. All feedback is collated in real time and analysed by an independent third-party think tank. This means it’s impartial – by the people for the people. We’re analysing feedback every day, so services get feedback faster.

Great services are exemplified. Let’s say Otago Police Station has a higher rating than the national average. We find out why, and then share these findings with other stations throughout the country to help ALL police stations improve.

Insights are provided back to Government. Not only are we collecting data from the government and Kiwis throughout NZ, but we’re creating a VIRTUOUS DATA LOOP by providing insights back to the government to help improve services in NZ and save the government millions of dollars every year.
Data is shared back to Kiwis too. Visit ourvoice.org.nz regularly to see how services in our country are improving over time. Let’s be proud of our innovative, transparent culture – and our country-wide effort to make NZ a better place for everyone.


###Our Data Story

Police State geographical data. We’ve pulled data from NZ Police – geographical data that you can find on a map and select your local station
All other government service data. We would have liked to keep going! Given more time, we would pull geographical information for all other major government service providers and include them on this site so the public could select and give feedback for them.

Victim and offender data. We pulled this data so that we could overlay this with the geographical data - this would show correlations between bad ratings 
and the criminal activity that is happening in a particular area. 
Invite feedback. Charities data can be downloaded to retrieve officer email addresses and phone numbers (we were able to pull over 1,000,000 email addresses). These emails can be sent an email campaign requesting their feedback on dealings with the charities services department and IRD.
The virtuous circle of data and insights. Not only are we collecting data from the government and Kiwis throughout NZ, but we’re creating a VIRTUOUS DATA LOOP by providing insights back to the government to help improve services in NZ and save the government millions of dollars every year.
Our original pitch

###The premise

Not only have we hacked a solution from government data this weekend, we’re using this data to provide insights back to the government - to make NZ a better place for everyone.

###Background

Government has a captive market: us. They have a monopoly on public services. We as taxpayers don’t have much choice. For example, if we need the police, or if we need help with an immigration problem, we only have one choice in each instance - NZ Police, Immigration NZ, IRD etc.

We collectively pay over $66 billion to our Government each year, so we deserve the best service possible. We can’t shop around (like you can with electricity) so we should demand the best that Government can provide.

###The problem:

There's currently no independent platform to voice feedback to Government. So, right now, we as citizens don’t know how effective Government services are. Can they be made more effective? Are they already effective? There is currently no way to compare and assess (in a transparent and open arena) the performance of Government services.

###The objective

Help improve Government services for the benefit of everyone - including the government! To create a ‘Warrant of Fitness' for our Government that encourages social innovation, development and policy improvements that benefit our society and future generations.

###The solution

An independent digital platform that gives Kiwis 'a voice'. Collectively we can help bring about improved Government services. The ability for a citizen to quickly, easily and anonymously rate and comment on a Government service they have recently experienced.

###The pitch

For the purpose of GovHack and the limited time we have, we have applied the idea to one Government Department: NZ Police. This allows us to theme the concept, give the idea context and keep the thinking simple.

But this idea is easily scalable to every kind of government organisation...as long as we can access their data.

###The financial benefit

Even if this feedback platform even if we could improve things even just by 1%, this would save our country approximately $660 million every year! The result? HUGE benefits both the Government and the taxpayer. Over time this could even justify a reduction in tax rates, leaving every New Zealander with a little more money in their pocket each week.

###What happens to the data?

One possible idea is that the site is independently managed and operated. This non-profit organisation collects, measures and analyses the data (feedback). This is then used to assess which service or stage of a service is working well and not so well. An independent think-tank of experts in public policy could be a part of this process. These are the ones who ultimately propose amendments to services, in order to ensure taxpayers are getting the best service possible…. Measure, Learn, Iterate.

Data could also be analysed and compared geographically as well. For example, maybe the Police in Otago have much higher approval ratings with the local public, compared to the national average. Learnings like this could be taken from one or more high-achieving segments in any given service and applied nationally.