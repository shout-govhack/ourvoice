
var serviceData;
var serviceLocations = [];

$(function(){
	

	// Load police data
	$.ajax({
		url: "/public/json/police-stations.min.json",
//		beforeSend: function (xhr) {
//			xhr.overrideMimeType("text/json; charset=x-user-defined");
//		}
	})
	.done(function (data) {
		serviceData = data.nodes;
		$.each(data.nodes, function(index, node){
			serviceLocations.push({lat: node.node.Latitude, lng: node.node.Longitude});
		});
	});
});

var map;
var markersArray = [];
		
function initMap() {
	// Create a map object and specify the DOM element for display.
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -41.24, lng: 174.02},
		scrollwheel: false,
		zoom: 4,
		disableDefaultUI: true
	});
	
	// add a click event handler to the map object
	google.maps.event.addListener(map, "click", function(event)
	{
		// place a marker
		placeMarker(event.latLng);

		// display the lat/lng in your form's lat/lng fields
		document.getElementById("interaction_lat").value = event.latLng.lat();
//		document.getElementById('lat').innerHTML = event.latLng.lat();
		document.getElementById("interaction_lng").value = event.latLng.lng();
//		document.getElementById('lng').innerHTML = event.latLng.lng();
	});
}

function placeMarker(location) {
	// first remove all markers if there are any
	deleteOverlays();

	var marker = new google.maps.Marker({
		position: location, 
		map: map
	});

	// add marker in markers array
	markersArray.push(marker);

	//map.setCenter(location);
	find_closest_marker(location);
}

function rad(x) {return x*Math.PI/180;}

function find_closest_marker( source_location ) {

    var lat = source_location.lat();
    var lng = source_location.lng();
    var R = 6371; // radius of earth in km
    var distances = [];
    var closest = -1;
	
	
	
    for( i=0;i< serviceData.length; i++ ) {
        var mlat = serviceData[i].node.Latitude; //position.lat();
        var mlng = serviceData[i].node.Longitude; //position.lng();
        var dLat  = rad(mlat - lat);
        var dLong = rad(mlng - lng);
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        distances[i] = d;
        if ( closest == -1 || d < distances[closest] ) {
            closest = i;
        }
    }

	var html = ''; //serviceData[closest].node.Title + " " + serviceData[closest].node.StationType + "<br />";
	html += serviceData[closest].node.Address + ", ";

	if(serviceData[closest].node.Suburb.trim() !== serviceData[closest].node.Town){
		html += serviceData[closest].node.Suburb.trim();
		
		if(serviceData[closest].node.Town !== undefined){
			html += ", ";
		}
	}
	
	if(serviceData[closest].node.Town !== undefined){
		html += serviceData[closest].node.Town;
	}
	
//	console.log(serviceData[closest].node);

	document.getElementById('police_station').innerHTML = html;
	document.getElementById('service_location_id').value = serviceData[closest].node.nid;
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
	if (markersArray) {
		for (i in markersArray) {
			markersArray[i].setMap(null);
		}
		markersArray.length = 0;
	}
}

//function initMap() {
//	var map = new google.maps.Map(document.getElementById('map'), {
//		center: {lat: 41.28, lng: 174.77},
//		zoom: 10
//	});
//	var infoWindow = new google.maps.InfoWindow({map: map});
//
//	// Try HTML5 geolocation.
//	if (navigator.geolocation) {
//		navigator.geolocation.getCurrentPosition(function (position) {
//			var pos = {
//				lat: position.coords.latitude,
//				lng: position.coords.longitude
//			};
//
//			infoWindow.setPosition(pos);
//			infoWindow.setContent('Location found.');
//			map.setCenter(pos);
//		}, function () {
//			handleLocationError(true, infoWindow, map.getCenter());
//		});
//	} else {
//		// Browser doesn't support Geolocation
//		handleLocationError(false, infoWindow, map.getCenter());
//	}
//}
//
//function handleLocationError(browserHasGeolocation, infoWindow, pos) {
//	infoWindow.setPosition(pos);
//	infoWindow.setContent(browserHasGeolocation ?
//			'Error: The Geolocation service failed.' :
//			'Error: Your browser doesn\'t support geolocation.');
//}