
$(function(){
	var my_autoComplete = new autoComplete({
		selector: 'input[name="search"]',
		minChars: 1,
		source: function(term, suggest){
			term = term.toLowerCase();
			var choices = ['ACC', 'Inland Revenue', 'Police'];
			var matches = [];
			for (i=0; i<choices.length; i++)
				if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
			suggest(matches);
		},
		onSelect: function(e, term, item){
			$('#serviceSearch').attr('action', 'police/home');
		}
	});
	
	$('#serviceSearch button').click(function(){
//		if($('#serviceSearch').attr('action') !== '' && $('#serviceSearch').attr('action') !== undefined){
			$('#serviceSearch').attr('action', 'police/home');
			$('#serviceSearch').submit();
//		}
	});
});
	
 
